# BAIXAR VERSÃO WINDOWS COM MÁQUINA VIRTUAL

Estamos trabalhando na versão Windows. Enquanto não dispomos dela, disponibilizamos uma máquina virtual GNU/Linux que tem a versão GNU/Linux do LIBRASOffice. 

## Passo a passo para instalar a máquina virtual e a ferramenta com o LIBRASOffice

- Verificar se a arquitetura do computador é x64 bits. Caso contrário, não será possível continuar o processo. [Siga este tutorial](https://canaltech.com.br/windows/como-saber-se-o-windows-e-32-ou-64-bits/), se precisar. 

- Verificar a quantidade de memória ram da máquina onde ocorrerá a instalação. Essa informação será usada na construição da ferramente com o LIBRASOffice. 

- Fazer download do instalador da máquina virtual (VM) disponível neste repositório. Está no mesmo diretório que este documento com nome “VirtualBox-6.0.12-133076-Win(1).exe”

- Execute o instalador e abra o programa.

- Faça o download da ferramente “LIBRASOffice-Mint19-.ova”. O arquivo é grande, então pode demorar um pouco. 

- Após a instalação do Virtual Box (máquina virtual), abra o Virtual Box e na interface vá até o item de menu “Arquivo (F)”. Clique em “Importar appliance” e na interface que aparecer, selecione o arquivo da ferramente na pasta onde o download foi realizado. 

- Após selecionar, clique em “próximo”. Algumas informações de configuração da máquina virtual devem aparecer. 

- O item “Ram” deve aparecer com 4096mb. Altere esse valor para a metade do valor da memória ram da máquina onde está sendo realizada a instalação. 

- Prossiga sem alterar os demais itens. Aguarde.

- Ao final, aparecerá um ícone com a máquina criada com nome “LinuxMint19”. Execute a máquina.

- Na área de trabalho há o ícone do LIBRASOffice. Execute e o programa estará disponível para uso. 

- A senha da Linux Mint da máquina virtual é “labiscoito”.

#### LOG

Alteração em 31/10/2019 por Lidiana
